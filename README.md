# sisop-praktikum-modul-1-2023-AM-C09
### Anggota Kelompok C09
| Nama                         | NRP        |
| ---------------------------- | -----------|
| Yohanes Teguh Ukur Ginting   | 5025211179 |
| Shazia Ingeyla Naveeda       | 5025211203 |
| Farrela Ranku Mahhisa        | 5025211129 |
| Ahmad Hafiz Mahardika        | 5025201196 |

### 1. Soal 1
#### 1a.
```sh
awk -F ',' '$4 == "Japan" ' datauniv.csv | sort -t ',' -k1 -n | head -5 | awk -F ',' '{print $1, $2, $4}'
```
- ``awk`` digunakan untuk memproses teks berformat tabel dengan baris dan kolom. Option `` -F ',' `` mengatur pemisah field dalam file menggunakan koma. `` $4 == "Japan" `` digunakan untuk memilih baris yang hanya memiliki nilai "Japan" pada kolom ke-4. Menggunakan operasi `` | `` hasil dari perintah ini akan dijadikan input bagi perintah selanjutnya begitu juga seterusnya. 

- ``sort -t ',' -k1 -n `` akan melakukan sorting pada data yang dihasilkan dari perintah sebelumnya berdasarkan kolom ke-1 dan akan di urutkan secara numerik berdasarkan option `` -n ``. Option `` -t ','`` digunakan untuk mengatur pemisah field pada file menggunakan koma dan diurutkan secara acsending.

- ``head -5`` command yang akan mengambil 5 baris pertama dari hasil output perintah sebelumnya.

- `` awk -F ',' '{print $1, $2, $4}' `` seluruh data pada kolom ke-1, ke-2, dan ke-4 yang sudah di sorting pada perintah-perintah sebelumnya akan ditampilkan.


#### 1b.
```sh
awk -F ',' '$4 == "Japan"' datauniv.csv | sort -t ',' -k1 -n | head -5 | sort -t ',' -k9 -n | head -1 |awk -F ',' '{print $1,$2,$4,$9}'
```
- ``awk`` digunakan untuk memproses teks berformat tabel dengan baris dan kolom. Option `` -F ',' `` mengatur pemisah field dalam file menggunakan koma. `` $4 == "Japan" `` digunakan untuk memilih baris yang hanya memiliki nilai "Japan" pada kolom ke-4. Menggunakan operasi `` | `` hasil dari perintah ini akan dijadikan input bagi perintah selanjutnya begitu juga seterusnya.

- ``sort -t ',' -k1 -n `` akan melakukan sorting pada data yang dihasilkan dari perintah sebelumnya berdasarkan kolom ke-1 dan akan di urutkan secara numerik berdasarkan option `` -n ``. Option `` -t ','`` digunakan untuk mengatur pemisah field pada file menggunakan koma dan diurutkan secara acsending.

- ``head -5`` command yang akan mengambil lima baris pertama dari hasil output perintah sebelumnya.

- Dilakukan sorting kembali pada perintah `` sort -t ',' -k9 -n `` berdasarkan kolom ke-9.

- ``head -1`` command yang akan mengambil hanya baris pertama dari hasil output perintah sebelumnya.

- `` awk -F ',' '{print $1,$2,$4,$9}' `` seluruh data pada kolom ke-1, ke-2, ke-4, dan ke-9 yang sudah di sorting pada perintah-perintah sebelumnya akan ditampilkan.


#### 1c.
```sh
awk -F ',' '$4 == "Japan"' datauniv.csv | sort -t ',' -k20n | head -10 | awk -F ',' '{print $1, $2, $4, $20}'
```
- ``awk`` digunakan untuk memproses teks berformat tabel dengan baris dan kolom. Option `` -F ',' `` mengatur pemisah field dalam file menggunakan koma. `` $4 == "Japan" `` digunakan untuk memilih baris yang hanya memiliki nilai "Japan" pada kolom ke-4. Menggunakan operasi `` | `` hasil dari perintah ini akan dijadikan input bagi perintah selanjutnya begitu juga seterusnya.

- ``sort -t ',' -k20n `` akan melakukan sorting pada data yang dihasilkan dari perintah sebelumnya berdasarkan kolom ke-20 dan akan di urutkan secara numerik berdasarkan option `` -k20n ``. Option `` -t ','`` digunakan untuk mengatur pemisah field pada file menggunakan koma dan diurutkan secara acsending.

- ``head -10`` command yang akan mengambil 10 baris pertama dari hasil output perintah sebelumnya.

- `` awk -F ',' '{print $1,$2,$4,$20}' `` seluruh data pada kolom ke-1, ke-2, ke-4, dan ke-20 yang sudah di sorting pada perintah-perintah sebelumnya akan ditampilkan.


#### 1d.
```sh
awk -F ',' '/Keren/ {print $1,$2}' datauniv.csv
```
- `` awk -F ',' '/Keren/ {print $1,$2}' `` digunakan untuk memilih baris yang hanya memiliki string "Keren" pada kolom berapapun dan akan menampilkan kolom pertama dan kolom kedua yang memenuhi ketentuan yang telah diberikan.



## Nomor 2
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

### Soal A
Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 

### Solusi :
**Implementasi Code Lengkap**
```sh
folder(){
#Menentukan jam saat ini dan jumlah file yang akan di download
now=$(date +"%H")
if [ $now -eq 0 ]; then
    X=1
else
    X=$now
fi

#Membuat folder dengan ketentuan nama kumpulan_
let jmlh_fldr=$(ls -d kumpulan_* | wc -l)+1
folder_name="kumpulan_$jmlh_fldr"
mkdir $folder_name

#Mendownload gambar dan menyimpan filenya
for((i=1; i<=X ;i++)) 
do
    name_file="perjalanan_$i.FILE"
    wget -O "$folder_name/$name_file" https://source.unsplash.com/1600x900/?indonesia

done
}
```
**Penjelasan :**

```sh
folder(){
    ...
}
```
- Pada solusi akan dibuat function agar dapat dipanggil per function dicronjob sesuai dengan ketentuan soal 2a dan 2b bahwa akan dipanggil setiap 10 jam dan 24 jam

```sh
now=$(date +"%H")
```
- untuk mendapatkan waktu saat ini sesuai ketentuan soal yaitu yang diambil adalah jam nya lalu disimpan di variabel now

```sh
if [ $now -eq 0 ]; then
    X=1
else
    X=$now
fi
```
- Untuk menentukan berapa gambar yang akan di download bila jam saat ini adalah 00 maka yang di download adalah 1 dan jika tidak maka banyak gambar yang akan di download adalah sebanyak jam saat ini lalu disimpan dalam variabel `x`
```sh
let jmlh_fldr=$(ls -d kumpulan_* | wc -l)+1`
folder_name="kumpulan_$jmlh_fldr"
```
- Digunakan untuk menentukan penamaan folder yaitu `kumpulan_[angka]`, folder yang baru akan dibuat adalah +1 dari folder yang sudah ada, bila belum ada maka dimulai dari 1

```sh
for((i=1; i<=X ;i++)) 
do
    name_file="perjalanan_$i.FILE"
    wget -O "$folder_name/$name_file" https://source.unsplash.com/1600x900/?indonesia

done
}
```
- Dilakukan perulangan untuk mendownload gambar sebanyak `x`  yaitu jam saat ini lalu

```sh
name_file="perjalanan_$i.FILE"
```

- Dilakukan penentuan penamaan file yang akan di download yaitu `perjalanan_[angka].FILE` dimana angka akan menyesuaikan dengan banyak file yang akan didownload dan akan mengikuti penomoran yang dimulai dari 1 sampai dengan banyak gambar yang akan di download yang disimpan dalam `x` lalu untuk penamaan akan menggunakan variabel `i` yang ada pada perulangan sehingga penamaan dimulai dari 1 sampai dengan `x`

```sh
wget -O "$folder_name/$name_file" https://source.unsplash.com/1600x900/?indonesia
```
- Lalu gambar akan di download dari source `https://source.unsplash.com/1600x900/?indonesia` dengan menggunakan fungsi `wget -O` memiliki syntax `wget -O <nama file saat di download> <sumber doownload>` dimana menggunakan option `-O` akan mendownload file dan menyimpan dengan penamaan `"$folder_name/$name_file"`

Lalu untuk memanggil fungsi folder untuk persoalan 2a dapat diberikan ketentuan

```sh
if [ "$1" = "run" ]; then
  folder
```

lalu sesuai ketentuan soal script akan berjalan setiap 10 jam sehingga akan menggunakan cronjob dengan syntax cronjob :

```sh
0 */10 * * * $(pwd)/kobeni_liburan.sh run
```
Lalu bisa juga diberikan command baru untuk langsung menambah cronjob ke dalam crontab: 
```sh
(crontab -l ; echo 0 */10 * * * "$(pwd)/kobeni_liburan.sh run" ) | crontab -
```

### Soal B
Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

### Solusi :
**Implementasi Code Lengkap**
```sh
ngezip(){
#Mendapatkan banyak zip dan banyak folder
let jml_zip=$(ls -d devil_* | wc -l)+1
zip_name="devil_$jml_zip.zip"
jml_fldr=$(ls -d kumpulan_* | wc -l)

for ((i=1; i <= jml_fldr; i++)) do
	zip -r $zip_name kumpulan_$i
	rm -r kumpulan_$i
done
}
```
**Penjelasan:**
```sh
ngezip(){
    ...
}
```
- Pada solusi akan dibuat function ngezip untuk zip agar dapat dipanggil per function dicronjob sesuai dengan ketentuan soal 2a dan 2b bahwa akan dipanggil 24 jam
```sh
let jml_zip=$(ls -d devil_* | wc -l)+1
zip_name="devil_$jml_zip.zip"
```
- Menentukan banyak file yang memiliki file dengan penamaan `devil_[angka]` sesuai dengan ketentuan penamaan zip file pada soal yaitu `devil_[angka].zip` lalu untuk penamaan nama zip akan ditambah 1 sehingga apabila diawal tidak ada zip file maka dimulai dari 1. Lalu akan disimpan kedalam variabel `zip_name`. `ls -d devil_*` digunakan untuk mencari dan menampilkan seluruh nama file dengan nama `devil_[angka]` dan ditampilkan perbaris. Lalu `wc -l` digunakan untuk menghitung banyak baris yang dioutput dari `ls -d devil_*`.

```sh
jml_fldr=$(ls -d kumpulan_* | wc -l)
```
- Menentukan banyak folder yang ada di dalam directory dengan `ls -d kumpulan_*` dimana akan mengeluarkan perbaris nama file yang memiliki ketentuan `kumpulan_[angka]` lalu banyak baris yang dioutput dihitung dengan `wc -l`lalu disimpan di `jml_fldr`

```sh
for ((i=1; i <= jml_fldr; i++)) do
	zip -r $zip_name kumpulan_$i
	rm -r kumpulan_$i
done
```
- Lalu akan dilakukan perulangan untuk melakukan zip folder yang ada didalam sebanyak `jml_fldr` dari 1 sampai dengan `jml_fldr`

```sh
zip -r $zip_name kumpulan_$i
```
- Akan melakukan zip dengan penamaan yang disimpan di variabel `zip_name` dimana `devil_[angka]` lalu pada fungsi `zip -r` akan melakukan zip dengan syntax `zip -r <nama file zip> <file yang mau di zip>` dimana dengan option `-r` akan melakukan zip terhadap folder dengan seluruh isi folder yang ada didalamnya lalu folder yang akan di zip memiliki ketentuan nama `kumpulan_$i` dimana `$i` menggunakan perulangan dari 1 sampai dengan banyak folder yang ada 

- Perulangan tersebut akan menambahkan folder yang akan dizip ke dalam zip yang sudah dibuat

Lalu agar script dapat dijalan untuk melakukan zip setiap 1 hari / 24 setelah dijalan maka dapat diberikan cronjob
```sh
0 0 * * * $(pwd)/kobeni_liburan.sh zip
```

Lalu bisa juga diberikan command baru untuk langsung menambah cronjob ke dalam crontab:

```sh
(crontab -l ; echo 0 0 * * * "$(pwd)/kobeni_liburan.sh zip" ) | crontab -
```
Lalu untuk memanggil funsi `ngezip` untuk persoalan 2b dapat diberikan ketentuan 

```sh
if [ "$1" = "zip" ]; then
  ngezip
```
Lalu untuk memanggil fungsi tadi dapat digabungkan menjadi
```sh
if [ "$1" = "run" ]; then
  folder
elif [ "$1" = "zip" ]; then
  ngezip
fi
```
**Kode Lengkap:**
```sh
#!/bin/bash
#Soal 2a dan 2b

ngezip(){
#Mendapatkan banyak zip dan banyak folder
let jml_zip=$(ls -d devil_* | wc -l)+1
zip_name="devil_$jml_zip.zip"
jml_fldr=$(ls -d kumpulan_* | wc -l)

for ((i=1; i <= jml_fldr; i++)) do
	zip -r $zip_name kumpulan_$i
	rm -r kumpulan_$i
done

}

folder(){
#Menentukan jam saat ini dan jumlah file yang akan di download
now=$(date +"%H")
if [ $now -eq 0 ]; then
    X=1
else
    X=$now
fi

#Membuat folder dengan ketentuan nama kumpulan_
let jmlh_fldr=$(ls -d kumpulan_* | wc -l)+1
folder_name="kumpulan_$jmlh_fldr"
mkdir $folder_name

#Mendownload gambar dan menyimpan filenya
for((i=1; i<=X ;i++)) 
do
    name_file="perjalanan_$i.FILE"
    wget -O "$folder_name/$name_file" https://source.unsplash.com/1600x900/?indonesia

done
}

if [ "$1" = "run" ]; then
  folder
elif [ "$1" = "zip" ]; then
  ngezip
fi

#CRONTAB COMMENT : untuk 10 jam sekali
#crontab -e
#0 */10 * * * $(pwd)/kobeni_liburan.sh run
#0 0 * * * $(pwd)/kobeni_liburan.sh zip



# Menambahkan cronjob ke crontab
#(crontab -l ; echo 0 */10 * * * "$(pwd)/kobeni_liburan.sh run" ) | crontab -
#(crontab -l ; echo 0 0 * * * "$(pwd)/kobeni_liburan.sh zip" ) | crontab -

```
### 3. Soal 3

**a. louis.sh**
Soal sudah sangat jelas, dengan peraturan password saat register :
1. Harus alphanumeric
2. Mengandung minimal 1 huruf kapital dan 1 huruf kecil
3. Tidak boleh sama dengan username
4. Tidak mengandung kata 'chicken' dan 'ernie'
5. Memiliki minimal 8 karakter
```
read -p "Username : " uname
read -s -p  "Password : " pword
```
Digunakan untuk membaca username dan password untuk register, lalu dimasukkan ke variabel uname untuk username, dan pword untuk password. Fungsi -s digunakan agar password tidak terlihat saat diketik

```bash
if grep -q "^un:$uname" users/users.txt
then
echo "Username dengan nama $uname sudah terdaftar!"
echo "$(date  +"%Y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" >> log.txt
exit 0
fi
```bash
Digunakan untuk mengecek pada users.txt apakah variabel uname sudah pernah atau belum, bila sudah, maka log error register akan dicatat ke log.txt. Program akan exit

```bash
if [ "$uname" = "$pword" ]
then
echo "Password tidak boleh sama dengan Username!"
exit 0
fi
```
Digunakan untuk mengecek apakah variabel uname sama dengan pword atau tidak, bila sama, maka akan mengeluarkan print error dan exit
```bash
if [[ $pword == "chicken" || $pword == "ernie" ]]
then
echo "Password tidak boleh mengandung kata 'chicken' atau 'ernie' ! "
exit 0
fi
```
Digunakan untuk mengecek apakah variabel uname adalah chicken atau ernie
```bash
if [ ${#pword} -lt 8 ]
then
echo "Password harus memiliki minimal 8 karakter!"
exit 0
fi
```
Digunakan untuk mengecek apakah variabel uname memiliki panjang lebih dari 8 karakter

```bash
if [[ !($pword =~ [A-Z]) || !($pword =~ [a-z]) || !($pword =~ [0-9]) ]]
then
echo "Password harus mengandung minimal 1 huruf kapital, 1 huruf kecil, dan angka"
exit 0
fi
```
Digunakan untuk mengecek apakah variabel uname memiliki minimal 1 kapital, 1 huruf kecil, dan angka.
```bash
echo "un:$uname pw:$pword" >> users/users.txt
echo "$(date  +"%Y/%m/%d %H:%M:%S") REGISTER: INFO User $uname registered successfully" >> log.txt
echo "Register Berhasil!"
echo "Selamat datang, $uname !"
```
Bila semua syarat password sudah terpenuhi, maka register akan berhasil dan tercatat pada log.txt





**b. retep.sh**
```bash
read -p "Username : " uname
read -s -p  "Password : " pword
```
Digunakan untuk membaca username dan password untuk register, lalu dimasukkan ke variabel uname untuk username, dan pword untuk password. Fungsi -s digunakan agar password tidak terlihat saat diketik
```bash
if grep -q "un:$uname pw:$pword" users/users.txt
then
echo "$(date +"%Y/%m/%d %H:%M:%S") LOGIN: INFO User $uname logged in" >> log.txt
echo "Login berhasil!"
else
echo "$(date +"%Y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $uname" >> log.txt
echo "Login gagal!"
exit 0
fi
```
Mengecek variabel uname dan pword apakah pernah terdaftar di log.txt, lalu mengecek juga apakah pasang uname dan pwordnya benar. Dalam kasus apapun, bila uname dan pword nya tidak ada di users.txt, maka login akan akan gagal dan tercatat ke log.txt lalu exit. Bila berhasil, maka akan tercatat berhasil di log.txt dan program exit

### 4. Soal 4
Berdasarkan soal, terdapat dua script yang harus dibuat, yaitu log_encrypt.sh dan log_decrypt.sh

Script log_encrypt.sh harus dapat melakukan :
1. Mem-back-up file syslog system
2. Mengkonversi karakter alphabet sesuai jam system.
3. Script berjalan setiap dua jam

**a. log_encrypt.sh**
```bash
awk 'BEGIN{for(n=0;n<256;n++)ord[sprintf("%c",n)]=n
        for(m=0;m<256;m++)chr[m]=sprintf("%c",m)}
```
Untuk menyelesaikan permasalahan dari soal, proses konversi karakter alphabet dapat dilakukan berdasarkan kode ASCII dari alphabet tersebut. Fungsi diatas berfungsi digunakan untul mendapatkan kode numeric ASCII dari alphabet.

```bash
hour=strftime("%H", systime())
```
Selanjutnya, perlu diketahui juga jam sistem saat itu. Fungsi diatas menyimpan jam sistem ke variabel ``hour``.

```bash
for (x = 1; x<=length(baris); x++) {
```
Karena cara kerja AWK adalah scanning file perbaris, maka diperlukan fungsi loop agar dapat scan per-character.

```bash
ascHuruf=ord[substr($0,x,1)]
```
Kode diatas berfungsi untuk memanggil fungsi ``ord`` untuk mendapatkan kode ASCII. Kode tersebut dimasukkan di variabel ``ascHuruf`` agar dapat diproses.

```bash
dupHour=hour
totalHourAsc=hour+ascHuruf
if ( (ascHuruf>64 && ascHuruf<=90) && totalHourAsc > 90) {
  hour-=(90-ascHuruf)
  hour=hour%26
  ascHuruf=64+hour
} else if ((ascHuruf>96 && ascHuruf<=122) && totalHourAsc>122){
  hour-=(122-ascHuruf)
  hour=hour%26
  ascHuruf=96+hour
} else if ((ascHuruf>64 && ascHuruf<=90) || (ascHuruf>96 && ascHuruf<=122)) {
  ascHuruf=ascHuruf+hour
}
```
Kode diatas adalah berfungsi untuk menambahkan ASCII alphabet dengan jam sistem. Sebelum proses penambahan perlu diketahui apakah setelah ditambahkan dengan jam sistem kode ASCII akan melebihi kode ASCII huruf z/Z dan juga kode ASCII alphabet harus dalam scope ASCII a-z/A-Z. Setelah diketahui syarat tersebut, maka pemrosesan kode ASCII akan berlangsung.

```bash
hour=dupHour
hurufBalik=chr[ascHuruf]
printf hurufBalik
```
Setelah diproses, kode ASCII harus dikonversikan kembali ke character alphabet yang sesuai. Proses konversi kembali dapat dilakukan dengan memanggil fungsi ``chr``. Setelah dikonversi ke character alphabet, character selanjutnya di-print.

```bash
}' ./var/log/syslog > $(date +"%H:%M %d:%m:%Y").txt
```
Kode diatas menunjukkan sumber data dan tujuan/lokasi output.

```bash
crontab -e
0 */2 * * * "$(pwd)/log_encrypt.sh"

#Atau
(crontab -l ; echo #0 */2 * * * "$(pwd)/log_encrypt.sh" ) | crontab -

```
Agar script berjalan secara otomatis setiap dua jam, maka dibutuhkan cronjob. Kode diatas berfungsi untuk menambahkan script ke crontab.

**b. log_decrypt.sh**

Script dekripsi pada dasarnya adalah kebalikan dari script enkripsi. Sehingga, perubahan pada script terdapat pada :
```bash
minusHourAsc=ascHuruf-hour
if ( (ascHuruf>64 && ascHuruf<=90) && minusHourAsc<=64) {
  hour-=(ascHuruf-64)
  hour=hour%26
  ascHuruf=90-hour
} else if ((ascHuruf>96 && ascHuruf<=122) &&              minusHourAsc<=96){
  hour-=(ascHuruf-96)
  hour=hour%26
  ascHuruf=122-hour
} else if ((ascHuruf>64 && ascHuruf<=90) || (ascHuruf>96 && ascHuruf<=122)) {
  ascHuruf=ascHuruf-hour
}
hour=dupHour
hurufBalik=chr[ascHuruf]
printf hurufBalik
```
Karena script enkripsi menambahkan ASCII dengan jam sistem, maka script decrypt mengurangi ASCII dengan jam sistem.

```bash
}' $(date +%H:%M_%d:%m:%Y).txt > Hasil_decrypt.txt
```
Kode diatas menunjukkan sumber data dan tujuan output.
