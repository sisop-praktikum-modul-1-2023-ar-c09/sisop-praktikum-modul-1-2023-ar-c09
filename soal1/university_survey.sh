#!/bin/bash

echo 1a
awk -F ',' '$4 == "Japan" ' datauniv.csv | sort -t ',' -k1 -n | head -5 | awk -F ',' '{print $1, $2, $4}'
echo

echo 1b
awk -F ',' '$4 == "Japan"' datauniv.csv | sort -t ',' -k1 -n | head -5 | sort -t ',' -k9 -n | head -1 |awk -F ',' '{print $1,$2,$4,$9}'
echo

echo 1c
awk -F ',' '$4 == "Japan"' datauniv.csv | sort -t ',' -k20n | head -10 | awk -F ',' '{print $1, $2, $4, $20}'
echo

echo 1d
awk -F ',' '/Keren/ {print $1,$2}' datauniv.csv
