#!/bin/bash
echo "REGISTER"
read -p "Username : " uname
read -s -p "Password : " pword

if grep -q "^un:$uname" users/users.txt
then
echo "Username dengan nama $uname sudah terdaftar!"
echo "$(date  +"%Y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" >> log.txt
exit 0
fi


if [ "$uname" = "$pword" ]
then
echo "Password tidak boleh sama dengan Username!"
exit 0
fi

if [[ $pword == "chicken" || $pword == "ernie" ]]
then
echo "Password tidak boleh mengandung kata 'chicken' atau 'ernie' ! "
exit 0
fi

if [ ${#pword} -lt 8 ]
then
echo "Password harus memiliki minimal 8 karakter!"
exit 0
fi


if [[ !($pword =~ [A-Z]) || !($pword =~ [a-z]) || !($pword =~ [0-9]) ]]
then
echo "Password harus mengandung minimal 1 huruf kapital, 1 huruf kecil, dan angka"
exit 0
fi


echo "un:$uname pw:$pword" >> users/users.txt
echo "$(date  +"%Y/%m/%d %H:%M:%S") REGISTER: INFO User $uname registered successfully" >> log.txt
echo "Register Berhasil!"
echo "Selamat datang, $uname !"
