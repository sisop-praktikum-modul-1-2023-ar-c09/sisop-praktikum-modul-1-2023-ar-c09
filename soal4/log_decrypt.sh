#!/bin/bash
#file script dan hasil enkripsi di root 
awk 'BEGIN{for(n=0;n<256;n++)ord[sprintf("%c",n)]=n
        for(m=0;m<256;m++)chr[m]=sprintf("%c",m)}
{
        baris=$0 #masukin baris ke array
        hour=strftime("%H", systime())
        for (x = 1; x<=length(baris); x++) {
                ascHuruf=ord[substr($0,x,1)]
                dupHour=hour
                minusHourAsc=ascHuruf-hour
                if ( (ascHuruf>64 && ascHuruf<=90) && minusHourAsc<=64) {
                        hour-=(ascHuruf-64)
                        hour=hour%26
                        ascHuruf=90-hour
                } else if ((ascHuruf>96 && ascHuruf<=122) && minusHourAsc<=96){
                        hour-=(ascHuruf-96)
                        hour=hour%26
                        ascHuruf=122-hour
                } else if ((ascHuruf>64 && ascHuruf<=90) || (ascHuruf>96 && ascHuruf<=122)) {
                        ascHuruf=ascHuruf-hour
                }
                hour=dupHour
                hurufBalik=chr[ascHuruf]
                printf hurufBalik
        }
        printf "\n"
}' $(date +%H:%M_%d:%m:%Y).txt > Hasil_decrypt.txt
