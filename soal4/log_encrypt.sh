#!/bin/bash
#File script di root
awk 'BEGIN{for(n=0;n<256;n++)ord[sprintf("%c",n)]=n
        for(m=0;m<256;m++)chr[m]=sprintf("%c",m)}
{

        baris=$0 #masukin baris ke array
        hour=strftime("%H", systime())
        for (x = 1; x<=length(baris); x++) {
                ascHuruf=ord[substr($0,x,1)]

                dupHour=hour
                totalHourAsc=hour+ascHuruf
                if ( (ascHuruf>64 && ascHuruf<=90) && totalHourAsc > 90) {
                        hour-=(90-ascHuruf)
                        hour=hour%26
                        ascHuruf=64+hour
                } else if ((ascHuruf>96 && ascHuruf<=122) && totalHourAsc>122){
                        hour-=(122-ascHuruf)
                        hour=hour%26
                        ascHuruf=96+hour
                } else if ((ascHuruf>64 && ascHuruf<=90) || (ascHuruf>96 && ascHuruf<=122)) {
                        ascHuruf=ascHuruf+hour
                }
                hour=dupHour
                hurufBalik=chr[ascHuruf]
                printf hurufBalik
        }
        printf "\n"
}' ./var/log/syslog > $(date +"%H:%M %d:%m:%Y").txt

#CRONTAB COMMENT :
#crontab -e
#0 */2 * * * "$(pwd)/log_encrypt.sh"

#Bisa juga langsung
#(crontab -l ; echo #0 */2 * * * "$(pwd)/log_encrypt.sh" ) | crontab -
