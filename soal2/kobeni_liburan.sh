#!/bin/bash
#Soal 2a dan 2b

ngezip(){
#Mendapatkan banyak zip dan banyak folder
let jml_zip=$(ls -d devil_* | wc -l)+1
zip_name="devil_$jml_zip.zip"
jml_fldr=$(ls -d kumpulan_* | wc -l)

for ((i=1; i <= jml_fldr; i++)) do
	zip -r $zip_name kumpulan_$i
	rm -r kumpulan_$i
done

}

folder(){
#Menentukan jam saat ini dan jumlah file yang akan di download
now=$(date +"%H")
if [ $now -eq 0 ]; then
    X=1
else
    X=$now
fi

#Membuat folder dengan ketentuan nama kumpulan_
let jmlh_fldr=$(ls -d kumpulan_* | wc -l)+1
folder_name="kumpulan_$jmlh_fldr"
mkdir $folder_name

#Mendownload gambar dan menyimpan filenya
for((i=1; i<=X ;i++)) 
do
    name_file="perjalanan_$i.FILE"
    wget -O "$folder_name/$name_file" https://source.unsplash.com/1600x900/?indonesia

done
}

if [ "$1" = "run" ]; then
  folder
elif [ "$1" = "zip" ]; then
  ngezip
fi

#CRONTAB COMMENT : untuk 10 jam sekali
#crontab -e
#0 */10 * * * $(pwd)/kobeni_liburan.sh run
#0 0 * * * $(pwd)/kobeni_liburan.sh zip



# Menambahkan cronjob ke crontab
#(crontab -l ; echo 0 */10 * * * "$(pwd)/kobeni_liburan.sh run" ) | crontab -
#(crontab -l ; echo 0 0 * * * "$(pwd)/kobeni_liburan.sh zip" ) | crontab -

